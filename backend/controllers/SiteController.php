<?php
namespace backend\controllers;

use common\components\vk\callbackApi\server\VKCallbackApiServerHandler;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Request;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @var VKCallbackApiServerHandler|object
     */
    private $vkCallbackApiServerHandler;

    public function init(): void
    {
        $this->vkCallbackApiServerHandler = Yii::createObject(VKCallbackApiServerHandler::class);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    protected function getRequest(): Request
    {
        return Yii::$app->getRequest();
    }

    protected function exit(): void
    {
        Yii::$app->end();
    }

    public function actionIndex(): void
    {
        $request = Json::decode($this->getRequest()->getRawBody(), false);

        $this->vkCallbackApiServerHandler->parse($request);

        $this->exit();
    }
}
