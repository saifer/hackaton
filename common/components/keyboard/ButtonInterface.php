<?php

namespace common\components\keyboard;

interface ButtonInterface
{
    /**
     * @return string
     */
    public function getMessage(): string;
}
