<?php

namespace common\components\keyboard;

use common\components\keyboard\individual\Index as IndividualIndex;
use common\components\keyboard\individualEntrepreneur\Index as IndividualEntrepreneurIndex;

/**
 * Class Start
 * @package common\components\keyboard
 * @link https://vk.com/dev/bots_docs_3?f=4.%2B%D0%9A%D0%BB%D0%B0%D0%B2%D0%B8%D0%B0%D1%82%D1%83%D1%80%D1%8B%2B%D0%B4%D0%BB%D1%8F%2B%D0%B1%D0%BE%D1%82%D0%BE%D0%B2
 */
class Start extends Button
{
    const STATE = 'start';

    protected function message(): string
    {
        return 'Начать бизнес легче, чем кажется! Давай я помогу тебе.
                Ты собираешься вести бизнес один или с партнером?';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Начать',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }

    protected function buttons(): array
    {
        return [
            IndividualIndex::class,
            IndividualEntrepreneurIndex::class,
        ];
    }

}
