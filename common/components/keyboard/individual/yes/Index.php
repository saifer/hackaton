<?php

namespace common\components\keyboard\individual\yes;

use common\components\keyboard\Button;

use common\components\keyboard\individualEntrepreneur\otherForm\Index as OtherForm;

class Index extends Button
{
    const STATE = 'state_individualEnt_yes';

    protected function message(): string
    {
        return 'Думаю, тебе следует зарегистрировать ООО. [Описание организационной формы]';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Да',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_POSITIVE;
    }

    protected function buttons(): array
    {
        return [
            OtherForm::class,
        ];
    }
}