<?php

namespace common\components\keyboard\individual\yes\otherForm;

use common\components\keyboard\Button;
use common\components\keyboard\individualEntrepreneur\otherForm\Index as IndividualEntrepreneurOtherForm;

class Index extends Button
{
    const STATE = 'state_individual_yes_otherForm';

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Да',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_POSITIVE;
    }

    protected function buttons(): array
    {
        return [
            IndividualEntrepreneurOtherForm::class,
        ];
    }
}
