<?php

namespace common\components\keyboard\individual;

use common\components\keyboard\Button;
use common\components\keyboard\individual\no\Index as NoIndex;
use common\components\keyboard\individual\yes\Index as YesIndex;

class Index extends Button
{
    const STATE = 'state_individual';

    protected function message(): string
    {
        return 'Много ли у тебя будет денежных обязательств?
                Например, ты будешь заказывать крупные партии товара,
                арендовать дорогостоящее имущество и т.п.?';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Один',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }

    /**
     * @return array
     */
    protected function buttons(): array
    {
        return [
            YesIndex::class,
            NoIndex::class,
        ];
    }
}
