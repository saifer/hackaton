<?php

namespace common\components\keyboard\individual\no\next\spare\moreDetailed;

use common\components\keyboard\Button;

class Index extends Button
{
    const STATE = 'state_moreDetailed';

    protected function message(): string
    {
        return 'https://www.nalog.ru/';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Подробнее',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }
}