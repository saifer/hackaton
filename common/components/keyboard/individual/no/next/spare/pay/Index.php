<?php

namespace common\components\keyboard\individual\no\next\spare\pay;

use common\components\keyboard\Button;

class Index extends  Button
{
    const STATE = 'state_payInd';

    protected function message(): string
    {
        return 'https://www.nalog.ru/';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Оплатить',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }
}