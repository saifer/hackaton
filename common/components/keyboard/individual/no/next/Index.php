<?php

namespace common\components\keyboard\individual\no\next;

use common\components\keyboard\Button;

use common\components\keyboard\individual\no\next\spare\Index as SpareIndex;

use common\components\keyboard\individual\no\next\stateDuty\Index as StateDutyIndex;

use common\components\keyboard\individual\no\next\statement\Index as StatementIndex;

class Index extends Button
{
    const STATE = 'state_individual_no_next';

    protected function message(): string
    {
        return 'Итак, для регистрации ИП тебе потребуется: заявление, копия паспорта, и оплата госпошлины.
         А еще я могу рассказать как сэкономить на оплате госпошлины!';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Далее',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_POSITIVE;
    }

    protected function buttons(): array
    {
        return[
            SpareIndex::class,
            StateDutyIndex::class,
            StatementIndex::class,
        ];
    }
}