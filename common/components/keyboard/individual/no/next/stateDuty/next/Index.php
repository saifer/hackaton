<?php

namespace common\components\keyboard\individual\no\next\stateDuty\next;

use common\components\keyboard\Button;

class Index extends Button
{
    const STATE = 'state_IndexNext';

    protected function message(): string
    {
        return 'Осталось подать готовый пакет документов в ФНС. Это можно сделать в электронном формате с помощью ЭЦП или самостоятельно, посетив МФЦ или отделение ФНС.';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Далее',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_POSITIVE;
    }
}