<?php

namespace common\components\keyboard\individual\no\next\stateDuty;

use common\components\keyboard\Button;
use common\components\keyboard\individual\no\next\stateDuty\next\Index as NextIndex;
use common\components\keyboard\individual\no\next\stateDuty\pay\Index as PayIndex;
use common\components\keyboard\individual\no\next\stateDuty\spare\Index as SpareIndex;

class Index extends Button
{
    const STATE = 'state_stateDuty';

    protected function message(): string
    {
        return 'Необходимо уплатить госпошлину в размере 800 рублей.
         Это можно сделать онлайн на сайте ФНС РФ:
         https://service.nalog.ru/gosreg/Также можно сэкономить на госпошлине!';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Госпошлина',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }

    protected function buttons(): array
    {
        return[
            NextIndex::class,
            PayIndex::class,
            SpareIndex::class,
        ];
    }
}