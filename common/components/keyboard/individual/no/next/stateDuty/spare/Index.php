<?php

namespace common\components\keyboard\individual\no\next\stateDuty\spare;

use common\components\keyboard\Button;

use common\components\keyboard\individual\no\next\stateDuty\next\Index as NextIndex;

use common\components\keyboard\individual\no\next\stateDuty\spare\moreDetailed\Index as MoreDetailedIndex;

class Index extends Button
{
    const STATE = 'state_stateDutyIndex';

    protected function message(): string
    {
        return 'С 01.01.2019 при направлении документов для государственной регистрации в форме электронных документов,
         в том числе через МФЦ и нотариуса, уплачивать государственную пошлину не требуется!';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Сэкономить',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }

    protected function buttons(): array
    {
        return [
                NextIndex::class,
        MoreDetailedIndex::class,

        ];
    }
}