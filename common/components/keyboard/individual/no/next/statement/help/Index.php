<?php

namespace common\components\keyboard\individual\no\next\statement\help;

use common\components\keyboard\Button;

class Index extends Button
{
    const STATE = 'state_help';

    protected function message(): string
    {
        return 'Какие у тебя полные фамилия имя и отчество (если есть)';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Помочь',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }
}