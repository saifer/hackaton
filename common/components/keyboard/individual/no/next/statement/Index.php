<?php

namespace common\components\keyboard\individual\no\next\statement;

use common\components\keyboard\Button;

class Index extends Button
{
    const STATE = 'state_statement';

    protected function message(): string
    {
        return 'Заявление должно быть заполнено согласно официальной утвержденной форме.
                Я также могу помочь тебе с заполнением, если хочешь!';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Заявление',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }
}