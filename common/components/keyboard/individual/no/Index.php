<?php

namespace common\components\keyboard\individual\no;

use common\components\keyboard\Button;
use common\components\keyboard\individual\no\next\Index as NextIndex;
use common\components\keyboard\individualEntrepreneur\otherForm\Index as IndividualEntrepreneurOtherForm;

class Index extends Button
{
    const STATE = 'state_individual_no';

    protected function message(): string
    {
        return 'Вероятно, тебе следует зарегистрироваться в качестве индивидуального предпринимателя. [Описание организационной формы]';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Нет',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_NEGATIVE;
    }

    protected function buttons(): array
    {
        return [
            NextIndex::class,
            IndividualEntrepreneurOtherForm::class,
        ];
    }
}