<?php

namespace common\components\keyboard\individualEntrepreneur\next;

use common\components\keyboard\Button;

class Index extends Button
{
    const STATE = 'state_nextIndex';

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Далее',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_POSITIVE;
    }
}