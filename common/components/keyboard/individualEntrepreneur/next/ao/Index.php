<?php

namespace common\components\keyboard\individualEntrepreneur\next\ao;

use common\components\keyboard\Button;

class Index extends Button
{
    const STATE = 'state_ao';

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'АО',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }
}