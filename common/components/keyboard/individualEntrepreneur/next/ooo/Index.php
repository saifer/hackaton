<?php

namespace common\components\keyboard\individualEntrepreneur\next\ooo;

use common\components\keyboard\Button;

class Index extends Button
{
    const STATE = 'state_ooo';

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'ООО',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }
}