<?php

namespace common\components\keyboard\individualEntrepreneur\next\ip;

use common\components\keyboard\Button;

class Index extends Button
{
    const STATE = 'state_ip';

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'ИП',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }
}