<?php

namespace common\components\keyboard\individualEntrepreneur;

use common\components\keyboard\Button;
use common\components\keyboard\individualEntrepreneur\next\Index as NextIndex;
use common\components\keyboard\individualEntrepreneur\otherForm\Index as OtherFormIndex;

class Index extends Button
{
    const STATE = 'state_individualEntrepreneurIn';

    protected function message(): string
    {
        return 'Думаю, тебе следует зарегистрировать ООО. [Описание организационной формы]';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'С партнёром',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }

    protected function buttons(): array
    {
        return[
            NextIndex::class,
            OtherFormIndex::class,
        ];
    }
}
