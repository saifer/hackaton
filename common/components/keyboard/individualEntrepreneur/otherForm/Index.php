<?php

namespace common\components\keyboard\individualEntrepreneur\otherForm;

use common\components\keyboard\Button;

use common\components\keyboard\individualEntrepreneur\next\ao\Index as AoIndex;
use common\components\keyboard\individualEntrepreneur\next\ip\Index as IpIndex;
use common\components\keyboard\individualEntrepreneur\next\ooo\Index as OooIndex;

class Index extends Button
{
    const STATE = 'state_otherFormIndex';

    protected function message(): string
    {
        return 'О какой организационной форме ты бы хотел узнать?';
    }

    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Узнать о других формах',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_PRIMARY;
    }

    protected function buttons(): array
    {
        return[
            AoIndex::class,
            IpIndex::class,
            OooIndex::class,
        ];
    }
}