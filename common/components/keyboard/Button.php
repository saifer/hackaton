<?php

namespace common\components\keyboard;

use yii\helpers\Json;

abstract class Button implements ButtonInterface
{
    const STATE = null;

    protected const TYPE_TEXT = 'text';

    protected const COLOR_PRIMARY = 'primary';
    protected const COLOR_SECONDARY = 'secondary';
    protected const COLOR_NEGATIVE = 'negative';
    protected const COLOR_POSITIVE = 'positive';

    /**
     * @var bool
     */
    protected $oneTime = false;
    /**
     * @var string
     */
    private $message;
    /**
     * @var string[]
     */
    private $action;
    /**
     * @var string
     */
    private $color;
    /**
     * @var string[]
     */
    private $buttons;

    public function __construct()
    {
        if (static::STATE === null) {
            throw new KeyboardException('Not set state');
        }

        $this->setMessage();
        $this->setAction();
        $this->setColor();
        $this->setButtons();
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    private function setMessage(): void
    {
        $this->message = $this->message();
    }

    protected function message(): string
    {
        return 'To be continued';
    }

    /**
     * @return array
     */
    public function getAction(): array
    {
        return $this->action;
    }

    private function setAction(): void
    {
        $this->action = array_merge($this->action(), [
            'payload' => $this->getPayload(),
        ]);
    }

    abstract protected function action(): array;

    public function getPayload(): string
    {
        return Json::encode(['command' => static::STATE]);
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    private function setColor(): void
    {
        $this->color = $this->color();
    }

    protected function color(): string
    {
        return self::COLOR_SECONDARY;
    }

    /**
     * @return string[]
     */
    public function getButtons(): array
    {
        return $this->buttons;
    }

    public function setButtons(): void
    {
        $this->buttons = array_merge($this->buttons(), [
            Index::class,
        ]);
    }

    protected function buttons(): array
    {
        return [];
    }

    /**
     * @return bool
     */
    public function isOneTime(): bool
    {
        return $this->oneTime;
    }
}
