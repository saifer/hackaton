<?php

namespace common\components\keyboard;

class Index extends Start
{
    protected function action(): array
    {
        return [
            'type' => self::TYPE_TEXT,
            'label' => 'Главное меню',
        ];
    }

    protected function color(): string
    {
        return self::COLOR_SECONDARY;
    }
}
