<?php

namespace common\components\vk\response;

class VKCallbackApiNewMessageResponse extends VKCallbackApiResponse
{
    const TYPE = 'message_new';
}
