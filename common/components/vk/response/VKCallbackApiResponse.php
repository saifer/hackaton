<?php

namespace common\components\vk\response;

use common\components\vk\response\object\BaseObject;
use yii\helpers\Json;

abstract class VKCallbackApiResponse
{
    const KEY_TYPE = 'type';
    const KEY_OBJECT = 'object';
    const KEY_SECRET = 'secret';
    const KEY_GROUP_ID = 'group_id';

    /**
     * @var string
     */
    private $type;

    /**
     * @var BaseObject
     */
    private $object;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var string
     */
    private $groupId;

    public function __construct(string $json)
    {
        $jsonObject = Json::decode($json, false);
        $this->setType($jsonObject);
        $this->setObject($jsonObject);
        $this->setSecret($jsonObject);
        $this->setSecret($jsonObject);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    protected function setType(object $json): void
    {
        $this->type = $json->{self::KEY_TYPE};
    }

    /**
     * @return BaseObject
     */
    public function getObject(): BaseObject
    {
        return $this->object;
    }

    /**
     * @param object $json
     */
    protected function setObject(object $json): void
    {
        $this->object = $json->{self::KEY_OBJECT};
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @param object $json
     */
    public function setSecret(object $json): void
    {
        $this->secret = $json->{self::KEY_SECRET};
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @param object $json
     */
    public function setGroupId(object $json): void
    {
        $this->groupId = $json->{self::KEY_GROUP_ID};
    }
}
