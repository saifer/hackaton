<?php

namespace common\components\vk;

use Yii;

class Config
{
    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var int
     */
    private $groupId;

    /**
     * @var string
     */
    private $callbackApiConfirmationToken;

    public function __construct()
    {
        $vkParams = Yii::$app->params['vk'];
        $this
            ->setAccessToken($vkParams['accessToken'])
            ->setSecret($vkParams['secret'])
            ->setGroupId($vkParams['groupId'])
            ->setCallbackApiConfirmationToken($vkParams['callbackApiConfirmationToken']);
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     *
     * @return Config
     */
    public function setAccessToken(string $accessToken): Config
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     *
     * @return Config
     */
    public function setSecret(string $secret): Config
    {
        $this->secret = $secret;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     *
     * @return Config
     */
    public function setGroupId(int $groupId): Config
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCallbackApiConfirmationToken(): string
    {
        return $this->callbackApiConfirmationToken;
    }

    /**
     * @param string $callbackApiConfirmationToken
     *
     * @return Config
     */
    public function setCallbackApiConfirmationToken(string $callbackApiConfirmationToken): Config
    {
        $this->callbackApiConfirmationToken = $callbackApiConfirmationToken;
        return $this;
    }
}
