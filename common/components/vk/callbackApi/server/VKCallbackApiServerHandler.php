<?php

namespace common\components\vk\callbackApi\server;

use common\components\keyboard\individual\Index as IndividualIndex;
use common\components\keyboard\individual\no\Index;
use common\components\keyboard\individual\no\next\Index as IndividualIndexNoNext;
use common\components\keyboard\individual\yes\Index as IndividualIndexYes;
use common\components\keyboard\individualEntrepreneur\Index as IndividualEntrepreneurIndex;
use common\components\keyboard\Start;
use common\components\vk\client\VKApiClient;
use common\components\vk\Config;
use common\components\vk\messageHandler\NewMessageHandler;
use VK\CallbackApi\Server\VKCallbackApiServerHandler as BaseVKCallbackApiServerHandler;
use yii\helpers\Json;

class VKCallbackApiServerHandler extends BaseVKCallbackApiServerHandler
{
    const SUCCESS_RESULT = 'ok';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var VKApiClient
     */
    private $VKApiClient;
    /**
     * @var NewMessageHandler
     */
    private $newMessageHandler;

    public function __construct(
        Config $config,
        VKApiClient $VKApiClient,
        NewMessageHandler $newMessageHandler
    ) {
        $this->config = $config;
        $this->VKApiClient = $VKApiClient;
        $this->newMessageHandler = $newMessageHandler;
    }

    /**
     * @param int $group_id
     * @param string|null $secret
     */
    function confirmation(int $group_id, ?string $secret)
    {
        if ($secret === $this->config->getSecret() && $group_id === $this->config->getGroupId()) {
            echo $this->config->getCallbackApiConfirmationToken();
        }
    }

    public function messageNew(int $group_id, ?string $secret, array $object)
    {
        $payload = $object['payload'] ?? null;
        $payload = Json::decode($payload) ?? null;

        $command = $payload['command'] ?? null;

        $keyboard = null;
        $message = null;

        switch ($command) {
            // start
            case Start::STATE:
                list($message, $keyboard) = $this->newMessageHandler->handle(new Start());
                break;
            // individual
            case IndividualIndex::STATE:
                list($message, $keyboard) = $this->newMessageHandler->handle(new IndividualIndex());
                break;
                // no
                case Index::STATE:
                    list($message, $keyboard) = $this->newMessageHandler->handle(new Index());
                    break;
                    // next
                    case IndividualIndexNoNext::STATE:
                        list($message, $keyboard) = $this->newMessageHandler->handle(new IndividualIndexNoNext());
                        break;
                        // spare
                        case \common\components\keyboard\individual\no\next\spare\Index::STATE:
                            list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\spare\Index());
                            break;
                            // moreDetailed
                            case \common\components\keyboard\individual\no\next\spare\moreDetailed\Index::STATE:
                            list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\spare\moreDetailed\Index());
                            break;
                            // next
                            case \common\components\keyboard\individual\no\next\spare\next\Index::STATE:
                                list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\spare\next\Index());
                                break;
                            // pay
                            case \common\components\keyboard\individual\no\next\spare\pay\Index::STATE:
                                list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\spare\pay\Index());
                                break;
                        // stateDuty
                        case \common\components\keyboard\individual\no\next\stateDuty\Index::STATE:
                            list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\stateDuty\Index());
                            break;
                            // next
                            case \common\components\keyboard\individual\no\next\stateDuty\next\Index::STATE:
                                list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\stateDuty\next\Index());
                                break;
                            // pay
                            case \common\components\keyboard\individual\no\next\stateDuty\pay\Index::STATE:
                                list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\stateDuty\pay\Index());
                                break;
                            // spare
                            case \common\components\keyboard\individual\no\next\stateDuty\spare\Index::STATE:
                                list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\stateDuty\spare\Index());
                                break;
                                // moreDetailed
                                case \common\components\keyboard\individual\no\next\stateDuty\spare\moreDetailed\Index::STATE:
                                    list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\stateDuty\spare\moreDetailed\Index());
                                    break;
                        // statement
                        case \common\components\keyboard\individual\no\next\statement\Index::STATE:
                            list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\statement\Index());
                            break;
                            // help
                            case \common\components\keyboard\individual\no\next\statement\help\Index::STATE:
                                list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individual\no\next\statement\help\Index());
                                break;
                    // otherForm
                    // common/components/vk/callbackApi/server/VKCallbackApiServerHandler.php:160
                // yes
                case IndividualIndexYes::STATE:
                    list($message, $keyboard) = $this->newMessageHandler->handle(new IndividualIndexYes());
                    break;
            // individualEntrepreneur
            case IndividualEntrepreneurIndex::STATE:
                list($message, $keyboard) = $this->newMessageHandler->handle(new IndividualEntrepreneurIndex());
                break;
                // next
                case \common\components\keyboard\individualEntrepreneur\next\Index::STATE:
                    list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individualEntrepreneur\next\Index());
                    break;
                    // ao
                    case \common\components\keyboard\individualEntrepreneur\next\ao\Index::STATE:
                        list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individualEntrepreneur\next\ao\Index());
                        break;
                    // ip
                    case \common\components\keyboard\individualEntrepreneur\next\ip\Index::STATE:
                        list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individualEntrepreneur\next\ip\Index());
                        break;
                    // ooo
                    case \common\components\keyboard\individualEntrepreneur\next\ooo\Index::STATE:
                        list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individualEntrepreneur\next\ooo\Index());
                        break;
                // otherForm
                case \common\components\keyboard\individualEntrepreneur\otherForm\Index::STATE:
                    list($message, $keyboard) = $this->newMessageHandler->handle(new \common\components\keyboard\individualEntrepreneur\otherForm\Index());
                    break;
            default:
                list($message, $keyboard) = $this->newMessageHandler->handle(new Start());
                break;
        }

        $params = [
            'random_id' => mt_rand(0, mt_getrandmax()),
            'peer_id' => $object['peer_id'] ?? null,
        ];

        if ($message !== null) {
            $params['message'] = $message;
        }

        if ($keyboard !== null) {
            $params['keyboard'] = $keyboard;
        }

        $this->VKApiClient
            ->messages()
            ->send(
                $this->config->getAccessToken(),
                $params
            );

        echo self::SUCCESS_RESULT;
    }
}
