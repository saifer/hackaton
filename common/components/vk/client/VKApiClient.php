<?php

namespace common\components\vk\client;

use VK\Client\VKApiClient as BaseVKApiClient;

class VKApiClient extends BaseVKApiClient
{
    const CALLBACK_API_EVENT_CONFIRMATION = 'confirmation';
    const CALLBACK_API_EVENT_MESSAGE_NEW = 'messageNew';
}
