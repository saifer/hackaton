<?php

namespace common\components\vk\messageHandler;

use common\components\keyboard\Button;
use yii\helpers\Json;

abstract class MessageHandler implements MessageHandlerInterface
{
    public function handle(Button $state): array
    {
        $keyboard = [
            'one_time' => $state->isOneTime(),
        ];

        foreach ($state->getButtons() as $buttonClass) {
            $button = $this->getButton($buttonClass);

            $keyboard['buttons'][][] = [
                'action' => $button->getAction(),
                'color' => $button->getColor(),
            ];
        }

        return [$state->getMessage(), Json::encode($keyboard)];
    }

    private function getButton(string $buttonClass): Button
    {
        return new $buttonClass;
    }
}
